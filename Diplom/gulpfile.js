const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const autoprefixer = require('gulp-autoprefixer');

var imageTask = function(){
  gulp.src('app/img/*')
      .pipe(image())
      .pipe(gulp.dest('dist/img'))
}

var autoprefixerTask = function() {
    return gulp.src('app/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
          browsers: ['last 2 versions'],
          cascade: false
        }))
        .pipe(gulp.dest('dist/css/'));
}

var fontsTask = function () {
    return gulp.src('app/fonts/*')
        .pipe(gulp.dest('dist/fonts'));
}

var watchTask = function () {
    gulp.watch('app/scss/*.scss', ['sass']);
    gulp.watch('app/img/*', ['image']);
    gulp.watch('app/fonts/*', ['fonts']);
}

gulp.task('image', imageTask);
gulp.task('sass', autoprefixerTask);
gulp.task('fonts', fontsTask);
gulp.task('watch', ['sass', 'image', 'fonts'], watchTask);

gulp.task('default', ['watch', 'fonts', 'image']);
